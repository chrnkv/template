$(function() {
	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});


$(document).on('scroll', function(ev) {
	var scrollTop = $(document).scrollTop(),
		$header = $('.js-header');

	if (scrollTop > 50) {
		$header.addClass('_scrolled');
	} else {
		$header.removeClass('_scrolled');
	}
});

$(".js-anchor").on('click', function(ev){
  var $link = $(ev.target).closest('a'),
  	  $href = $link.attr('href');
  	ev.preventDefault();
  	var fixed_offset = 80;
    $('html,body').stop().animate({ scrollTop: $($href).offset().top - fixed_offset }, 1000);
    ev.preventDefault();
});

$('.js-mobile-toggle').on('click', function(ev) {
	var	$btn = $(ev.target).closest('.js-mobile-toggle'),
		$mobileMenu = $('.js-mobile-menu');
		$btn.toggleClass('on');
	var isActive = $btn.hasClass('on');
	console.log(isActive)
	if (isActive) {
		$mobileMenu.fadeIn(200);
	} else {
		$mobileMenu.fadeOut(200)
	}
});

$(window).on('resize', function(ev) {
	if (window.innerWidth > 992) {
		var $btn = $('.js-mobile-toggle'),
			$mobileMenu = $('.js-mobile-menu');

		$btn.removeClass('on');
		$mobileMenu.hide();
	}
})