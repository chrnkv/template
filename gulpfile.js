var gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	prefix 		= require('gulp-autoprefixer'),
	plumber     = require('gulp-plumber'),
	browserSync = require('browser-sync').create(),
	rigger 		= require('gulp-rigger');
	dirSync     = require('gulp-directory-sync');


var assetsDir = 'assets/'; /* Исходники */
var outputDir = 'dist/';   /* Проект */
var buildDir  = 'build/';  /* Релиз */


// SVG
var svgSprite = require('gulp-svg-sprite'),
	svgmin = require('gulp-svgmin'),
	cheerio = require('gulp-cheerio'),
	replace = require('gulp-replace');

gulp.task('sass', function () {
	gulp.src([assetsDir + 'sass/**/*.scss', '!' + assetsDir + 'sass/**/_*.scss'])
		.pipe(plumber())
		.pipe(sass({outputStyle: 'expanded'}))
		.pipe(prefix('last 9 versions'))
		.pipe(gulp.dest(outputDir + 'css/'))
		.pipe(browserSync.stream());
});


gulp.task('html', function () {
    gulp.src([assetsDir + '*.html'])
    	.pipe(plumber())
        .pipe(rigger()) 
        .pipe(gulp.dest(outputDir)) 
        .pipe(browserSync.stream()); 
});


//-------------------------------------------------Synchronization

gulp.task('imageSync', function () {
	return gulp.src('')
		.pipe(plumber())
		.pipe(dirSync(assetsDir + 'img/', outputDir + 'img/', {printSummary: true}))
		.pipe(browserSync.stream());
});

gulp.task('fontsSync', function () {
	return gulp.src('')
		.pipe(plumber())
		.pipe(dirSync(assetsDir + 'fonts/', outputDir + 'fonts/', {printSummary: true}))
		.pipe(browserSync.stream());
});

gulp.task('jsSync', function () {
	return gulp.src(assetsDir + 'js/*.js')
		.pipe(plumber())
		.pipe(gulp.dest(outputDir + 'js/'))
		.pipe(browserSync.stream());
});

//-------------------------------------------------Synchronization###

// ----------------------------------------------- Watch and reload
gulp.task('watch', function () {
	gulp.watch(assetsDir + 'sass/**/*.scss', ['sass']);
	gulp.watch(assetsDir + 'js/**/*.js', ['jsSync']);
	gulp.watch(assetsDir + 'img/**/*', ['imageSync']);
	gulp.watch(assetsDir + '*.html', ['html']);
});


gulp.task('browser-sync', function () {
	browserSync.init({
		port: 1337,
		server: {
			baseDir: outputDir,
			index: 'index.html'
		}
	});
});


/* SVG SPITES */
gulp.task('svgSpriteBuild', function () {
	return gulp.src(assetsDir + 'img/svg/*.svg')
	// minify svg
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		// remove all fill, style and stroke declarations in out shapes
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: {xmlMode: true}
		}))
		// cheerio plugin create unnecessary string '&gt;', so replace it.
		.pipe(replace('&gt;', '>'))
		// build svg sprite
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: "../sprite.svg",
					render: {
						sass: {
							dest:'../../sass/_sprite.scss',
							template: assetsDir + "sass/templates/_sprite_template.scss"
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(assetsDir + 'img/'));
});


gulp.task('default', ['html', 'sass', 'imageSync', 'jsSync', 'watch', 'browser-sync']);

// ----------------------------------------------- Watch and reload###